package com.shop.tcd.model.settings

data class GroupUser(
    val userLogin: String,
    val userPassword: String,
)