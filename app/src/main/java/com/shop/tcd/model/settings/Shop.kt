package com.shop.tcd.model.settings

data class Shop(
    val shopName: String,
    val shopPrefix: String,
    val shopPrefixPiece: String,
    val shopPrefixWeight: String,
    val shopPrefixWeightPLU: String,
    val shopURL: String,
)