package com.shop.tcd.model

data class Groups(
    val group: List<Group>,
    val message: String,
    val result: String // success
)